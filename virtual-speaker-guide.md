# Cincinnati Software Craftsmanship Speaker Guide

Thanks for being a speaker at Cincinnati Software Craftsmanship! We've put together this guide to ensure that you have a great speaking experience.

## General Notes

### Meetup Audience
Cincinnati Software Craftsmanship covers a wide variety of topics and therefore attracts developers (and others) of various skill levels and specializations. We typically have 20-50 attendees at each meetup.

### Compensation
Regrettably, we are unable to offer monetary compensation to speakers at this time.

### Code of Conduct
Please reference the [Code of Conduct](/code-of-conduct.md) and keep it in consideration while preparing your talk. The organizers will be happy to answer any questions you may have about compliance with the Code of Conduct.

### Sponsors
The meetup currently has one continuous sponsor, Kroger Digital, that covers the meetup.com membership.

## 2 months before the talk or ASAP

### Talk Title
Please provide your talk title to enable us to begin promoting your talk. 

### Attend the meetup if you can
We love to give you the opportunity to begin promoting your talk as early as possible. We will announce your upcoming talk and give attendees a chance to RSVP.


## 1 month before the talk
### Talk description, bio, and photo
Provide a brief description of your talk, a personal bio, and a photo. _The earlier that you can provide these, the better._

You may want to reference the descriptions, bios, and photos for previous meetups if you're looking for inspiration.

Please be aware that Meetup.com only supports "plain text" for the meetup description. Styling markup, like Markdown, is not supporte. Thankfully, URLs will be rendered as links.

#### Description
This is simply a few sentences to help attendees decide if they are interested in your topic. The meetup organizers will be happy to provide some advice if you are struggling with your description. If possible, like a good story intro, it should grab people's attention and get them excited about your talk. Try to provide more than a sentence but less than a book.

#### Bio
Give a basic description of who you are, what you do, and what you enjoy. Like the description, a few sentences is a good length.

#### Photo 📸
Please provide a photo of yourself. We recommend that it be of decent quality and resolution. If you have any uncertainty we will be happy to advise. 😀

### Attend the meetup
We encourage speakers to attend the meetup prior to the event you will presenting at. Your talk will be promoted and we will give you the opportunity to say a little about your topic. 

## Promoting the Talk
The organizers will promote your meetup on social media and through a variety of other mediums. We encourage you to do so as well. If you have social media accounts, let us know and we will tag you in our posts. 


## Day of the Talk

### Rough Timeline
| Time | Event        |
|------|--------------|
| 5:30 | Pre-meeting Zoom begins |
| 5:45 | Join the Streamyard studio for final tech check |
| 6:00 | Stream begins. Short countdown, then intro from organizer(s) |
| 6:10 | Presentation begins |
| 7ish | Presentation ends |
| 5 min | Short outro and Giveaway |
|  | Post-meeting Zoom begins |


### Presentation Platform
CSC uses Streamyard to stream to Twitch and YouTube. It's a simple, browser-based streaming platform. We'll send you a link that will allow you to join the stream studio. We are happy to arrange a tech check before the meetup.

### Arrive Early
Please join the StreamYard studio by 5:45 to allow for final prep.

### Introduction by Organizers
We spend about five minutes at the beginning of the meetup to welcome everyone and make a few announcements, typically accompanied by a _very short_ slide deck. Sponsors will be thanked. We may also highlight a few upcoming developer community events. Finally, we will introduce you, the speaker. We typically will just introduce speakers by name but let us know if there's anything you'd like us to mention in your introductions.

### Talk Expectations
We recommend that your presentation last between 40 and 60 minutes.

## After the meetup

You did it! Thank you! Congratulations!

Your recorded talk will be shared via the [CSC YouTube channel](https://www.youtube.com/channel/UC5Cjdv38sS_yNEue-QUAF7g)

Here are some followup steps you can take.

### Share your materials
If you are willing, please post a link to your presention materials (slides, code, etc.) as a comment to the event on meetup.com.

### First time speaker? 😬 Keep speaking! 👏👏

Don't stop now! Public speaking is one of the most valuable things that you can do for yourself, your career, and the community. But you don't have to take my word for it. Check out what these other speakers say about public speaking:

[**Public Speaking Transformed My Life…and Can Change Yours Too** by Cory House][cory house public speaking]

[**Please, Give Public Speaking a Try** by David Neal][david neal public speaking]

There are nearly endless opportunities for speaking in the city, region, country, and around the world. 

Meetups are always looking for speakers: consider giving your talk again at another meetup, maybe in a nearby city.

Speaking at a conference is another great way to expand your experience and opportunities. Conference "Call for Speakers" (aka CfS or CFP) are open for specific periods of time but there is almost always one open or opening soon. You can add this [Conference CFP Calendar][cfp calendar] to your Google Calendar to stay up-to-date on conference CFPs in the region. If you would like to help contribute to the calendar, let us know!

Feel free to reach out if this interests you. We (the organizers) are always willing to provide advice in this area.


## Thank you!

We are delighted to have you speak at Cincinnati Software Craftsmanship. The meetup would not exist without wonderful speakers like yourself. 

If you have questions about any part of the process don't hesitate to let us know. 

If you have suggestions for improving this document, that's even better! We would love to hear your feedback.


[cfp calendar]: https://calendar.google.com/calendar?cid=NXNyY2Examc0cTZiZ2kxaDk2NXRpYnY2ZzhAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ
[cory house public speaking]: https://www.freecodecamp.org/news/public-speaking-transformed-my-life-and-can-change-yours-too-ca8acdbcc188/
[david neal public speaking]: https://reverentgeek.com/please-give-public-speaking-a-try/
