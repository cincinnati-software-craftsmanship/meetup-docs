# Sponsor Guide

## TL;DR

- Please read the entire document. All of the information is important and relevant.
- Provide a nice non-pizza dinner with vegetarian options
- Plan for 80% of RSVPs
- Providing soda as well is appreciated
- Send us a copy of your logo for the slide recognizing your sponsorship

## Opportunity
Cincinnati Software Craftsmanship currently offers the opportunity to sponsor the meetup by purchasing food for the monthly event. We see meetup sponsorship as beneficial on a variety of fronts, not simply as an advertising opportunity. Sponsors enhance their reputation for supporting the local developer community, increase name recognition, and have the opportunity to meet dozens of passionate Cincinnati developers. Meetup attendees get great food and the opportunity to make valuable contacts with sponsors.

## Benefits
Sponsors are recognized in the event details on meetup.com and in the announcements at the beginning of the meeting. Each month we present a quick slideshow of announcements just before the talk begins. As part of this slideshow, the food sponsor is thanked and given the opportunity to say a few words. **Please send us a copy of your logo so that we can recognize you for your sponsorship.**

Sponsorship also provides an excellent opportunity to connect with developers in the Cincinnati community. Attendees are typically more involved than the average developer. They tend to be passionate about personal career development, as evidenced by the fact that they've chosen to attend a meetup outside of work hours.

## Expectations

### The Meal
We ask that the food sponsor provide a nice non-pizza dinner and soda.  The meal should include options for vegetarians and should be set up by 5:30 PM. Consideration of other dietary restrictions (keto, gluten-free, vegan, etc.) is appreciated but _certainly not required_.

Below are a few of the excellent sponsored meals we have had catered so far:
- Chipotle
- Silver Ladle (Recommend ordering a bit more than normal)
- Gomez Salsa
- Maggiano's

Plates, bowls, and utensils as necessary should be provided with the meal. Most restaurants will include these on catering orders for free or a nominal charge.

The meeting space includes a water and ice machine but many attendees prefer soda. Either cans or 2-liters work well for the event.

### Quantity

Cincinnati Software Craftsmanship's monthly event currently ranges from around 40-60 RSVPs. Attendance averages approximately 80% of RSVPs, about 30 to 50 people, but does fluctuate somewhat (our max was over 92%) depending on a variety of factors.

We recommend that sponsors follow the meetup event page to track RSVPs for the sponsored event and provide food sufficient for at least 80% of the RSVP count. RSVPs can increase significantly in the days leading up to the event.


### The Representatives
We encourage sponsor representatives to attend the event and get to know some of the attendees. As mentioned before, we care about the relationship that we create between the meetup, attendees, and sponsors.

### Promotions

The organizers will promote the meetup on LinkedIn, Twitter, the Cincy Tech Slack and several other platforms. Any additional promotion or "signal boost" is much appreciated and will help to attract attendees to your sponsored event.

### Code of Conduct

All sponsors are, of course, expected to abide by the meetup [Code of Conduct](/code-of-conduct.md)