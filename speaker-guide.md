# Cincinnati Software Craftsmanship Speaker Guide

Thanks for being a speaker at Cincinnati Software Craftsmanship! We've put together this guide to ensure that you have a great speaking experience.

## General Notes

### Meetup Audience
Cincinnati Software Craftsmanship covers a wide variety of topics and therefore attracts developers (and others) of various skill levels and specializations. We typically have 20-50 attendees at each meetup.

### Compensation
Regrettably, we are unable to offer travel reimbursement or other compensation to speakers at this time.

### Code of Conduct
Please reference the [Code of Conduct](/code-of-conduct.md) and keep it in consideration while preparing your talk. The organizers will be happy to answer any questions you may have about compliance with the Code of Conduct.

### Sponsors
The meetup currently has one continuous sponsor, Kroger Digital, that provides the space for the meetup and, when we don't have a monthly sponsor, the food. Often, we also have a monthly sponsor who provides the food for that month's event. If you know anyone who might be interested in sponsoring the meetup (e.g. your company) please let us know.

## 2 months before the talk or ASAP

### Talk Title
Please provide your talk title to enable us to begin promoting your talk. 

### Attend the meetup if you can
We love to give you the opportunity to begin promoting your talk as early as possible. We will announce your upcoming talk and, if you are present, give you the opportunity to say a few words about the topic.

### Spotify Playlist
Once we have confirmed the date for your talk, we will send you a link to a Spotify playlist. Please add some of your favorite songs to the playlist. 

We will play these songs in the social time before and after the talk. The attendees love listening to some of the speaker's personal favorites. Please be reasonable with the content. 😎 We reserve the right to edit if you step _too far_ over the line.

#### Technical Details
You can use a free account to add songs but, unfortunately, you can only add songs to the playlist while logged in to the desktop or mobile app, not the web app. We understand that this is less than optimal and are examining other options for the playlist. If you have any difficulties or prefer an alternative method, you can make your own playlist and share it with us or simply send us a list of songs/artists that you enjoy. 

## 1 month before the talk
### Talk description, bio, and photo
Provide a brief description of your talk, a personal bio, and a photo. _The earlier that you can provide these, the better._

You may want to reference the descriptions, bios, and photos for previous meetups if you're looking for inspiration.

Please be aware that Meetup.com only supports "plain text" for the meetup description. Styling markup, like Markdown, is not supported. Thankfully, URLs will be rendered as links.

#### Description
This is simply a few sentences to help attendees decide if they are interested in your topic. The meetup organizers will be happy to provide some advice if you are struggling with your description. If possible, like a good story intro, it should grab people's attention and get them excited about your talk. Try to provide more than a sentence but less than a book.

#### Bio
Give a basic description of who you are, what you do, and what you enjoy. Like the description, a few sentences is a good length.

#### Photo
Please provide a photo of yourself _in landscape format_. We have to manually modify portait format photos and they _will_ look like they were edited by a developer (probably not good). meetup.com requests a photo "At least 1200 x 675 pixels" and we recommend that it be of decent quality and resolution. If you have any uncertainty we will be happy to advise.

### Attend the prior meetup if possible
We encourage speakers to attend the meetup prior to the event you will presenting at. Your talk will be promoted and we will give you the opportunity to say a little about your topic. 
People who have met the speaker have an extra incentive to attend your talk.


## Promoting the Talk
The organizers will promote your meetup on social media and through a variety of other mediums. We encourage you to do so as well. If you have social media accounts, let us know and we will tag you in our posts. 


## Day of the Talk

### Rough Timeline
| Time | Event        |
|------|--------------|
| 5:30 | Food is ready and attendees begin to arrive |
| 6:00 | Doors open to the presentation room |
| 6:15 | Formal meetup begins  |

### Meetup Location
The meetup is in the Kroger Digital meeting space on the first floor of the Atrium Two building. You can find the space on the south side of the lobby behind the glass wall.

### Parking
[Parking locations around Atrium Two can be found here.][parking]

### Room A/V
The room is equipped with HDMI for displaying your content on the televisions. We will have a USB-C-to-HDMI adapter available. If you need another type of adapter to connect your laptop to the HDMI cable (and you do not have access to one) then please let us know.

The room will hold approximately 60 people. There is no microphone/speaker system for the presenter but we have not found it to be necessary for the space.

The meetup space has a guest wifi network that you will be able to use. We have found it to be fairly reliable but, as always, it's advisable to plan for contingencies like the lack of an internet connection.

### Arrive Early
Please arrive by 5:30 to allow plenty of prep time. You can confirm that the A/V setup is working properly, grab some food, and chat with attendees.

### Introduction by Organizers
We spend about five minutes or less at the beginning of the formal meetup to welcome everyone and make a few announcements, typically accompanied by a _very short_ slide deck. Sponsors, including Kroger Digital and the monthly food sponsor (if we have one), will be thanked and given a moment to say who they are and what they do. We may also highlight a few upcoming developer community events. Finally, we will introduce you, the speaker. We typically will just introduce speakers by name but let us know if there's anything you'd like us to mention in your introductions.

### Talk Expectations
We recommend that your presentation last between 40 and 60 minutes.

## After the meetup

You did it! Thank you! Congratulations! 

Here are some followup steps you can take.

### Share your materials
If you are willing, please post a link to your presention materials (slides, code, etc.) as a comment to the event on meetup.com.

### Keep speaking!

Don't stop now! Public speaking is one of the most valuable things that you can do for yourself, your career, and the community. But you don't have to take my word for it. Check out what these other speakers say about public speaking:

[**Public Speaking Transformed My Life…and Can Change Yours Too** by Cory House][cory house public speaking]

[**Please, Give Public Speaking a Try** by David Neal][david neal public speaking]

There are nearly endless opportunities for speaking in the city, region, country, and around the world. 

Meetups are always looking for speakers: consider giving your talk again at another meetup, maybe in a nearby city.

Speaking at a conference is another great way to expand your experience and opportunities. Conference "Call for Speakers" (aka CfS or CFP) are open for specific periods of time but there is almost always one open or opening soon. You can add this [Conference CFP Calendar][cfp calendar] to your Google Calendar to stay up-to-date on conference CFPs in the region. If you would like to help contribute to the calendar, let us know!

Feel free to reach out if this interests you. We (the organizers) are always willing to provide advice in this area.


## Thank you!

We are delighted to have you speak at Cincinnati Software Craftsmanship. The meetup would not exist without wonderful speakers like yourself. 

If you have questions about any part of the process don't hesitate to let us know. 

If you have suggestions for improving this document, that's even better! We would love to hear your feedback.


[cfp calendar]: https://calendar.google.com/calendar?cid=NXNyY2Examc0cTZiZ2kxaDk2NXRpYnY2ZzhAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ
[cory house public speaking]: https://www.freecodecamp.org/news/public-speaking-transformed-my-life-and-can-change-yours-too-ca8acdbcc188/
[david neal public speaking]: https://reverentgeek.com/please-give-public-speaking-a-try/
[parking]: https://www.bestparking.com/cincinnati-oh-parking/destinations/atrium-two-parking/?daily=1
