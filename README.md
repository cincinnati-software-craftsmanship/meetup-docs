# Cincinnati Software Craftsmanship Meetup Docs

## [Code of Conduct](/code-of-conduct.md)

Code of Conduct governing all meetup activities.

## [Speaker Guide](/speaker-guide.md)

Relevant information for all meetup speakers or potential speakers.

## [Sponsor Guide](/sponsor-guide.md)

Information for meetup sponsors.

----------

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.